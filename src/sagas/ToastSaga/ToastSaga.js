import { put, delay } from 'redux-saga/effects';
import { ActResetToast, ActShowToast } from '../../redux/actions/ToastAction';

export function* pushToast(data) {
    let time = data?.payload?.time || 1000
    yield put(ActShowToast(data?.payload))
    if (data?.payload?.autoHide == true) {
        yield delay(time)
        yield put(ActResetToast())
    }
}