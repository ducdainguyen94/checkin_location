import AsyncStorage from '@react-native-community/async-storage';

const ReduxPersist = {
  active: true,
  reducerVersion: '0.2',
  storeConfig: {
    key: 'primary',
    storage: AsyncStorage,
    version: 2,
    blacklist: ['loading', 'userApp', 'down', 'toast', 'checkin', 'modal'],
  },
};

export default ReduxPersist;
