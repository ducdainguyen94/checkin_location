const MaxDistance = 50.0

export const LinkIntface = 'intface://intface.com/checkinscreen/'
export const KEY_QR = 'intface://intface.com'

const rad = function (x) {
    return x * Math.PI / 180;
};

const getDistance = function (lat, lng, latCompany, lngCompany) {
    var R = 6378137;
    var dLat = rad(lat - latCompany);
    var dLong = rad(lng - lngCompany);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(latCompany)) * Math.cos(rad(lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
};

export const CheckPositionIsInSideCompanyBound = (lat, lng, latCompany, lngCompany) => {
    const dis = getDistance(lat, lng, latCompany, lngCompany)
    if (dis <= MaxDistance) {
        return true
    }
    return false
}
