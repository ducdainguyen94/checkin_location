import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, Alert, Platform, Linking } from 'react-native'
import FastImage from 'react-native-fast-image'
import { useSelector, } from 'react-redux'
import { FONT_BOLD, FONT_SEMIBOLD, IC_UPDATE } from '../assets'
import { RFValue } from '../constants/multiScreen'
import ListColorsSystem from '../themes/ListColorsSystem'
import { VERSION_NAME } from '../utils/Contants'
import checkVersion from 'react-native-store-version';

// api
import Api from './../api/user'

const ViewCheckNewVersion = () => {
    const [isShowNewVersion, setIsShowNewVersion] = useState(false)
    const [detailVersionNew, setDetailVersionNew] = useState()

    const theme = useSelector(state => state.theme)

    useEffect(() => {
        checkNewVersion()
    }, [])

    useEffect(() => {
        if (detailVersionNew) {
            openNewVersion()
        }
    }, [detailVersionNew])

    const checkNewVersion = async () => {
        try {
            const check = await checkVersion({
                version: VERSION_NAME,
                iosStoreURL: 'https://apps.apple.com/vn/app/id1597884858',
                androidStoreURL: 'https://play.google.com/store/apps/details?id=com.intlabs.intface',
                country: 'vn'
            });

            if (check?.result === "new") {
                setDetailVersionNew(check)
            } else {
                closeNewVersion()
            }
        } catch (err) {
            console.log('checkNewVersion error', err)
        }
    }
    const closeNewVersion = () => setIsShowNewVersion(false)
    const openNewVersion = () => setIsShowNewVersion(true)

    const goToUpdate = () => {
        try {
            closeNewVersion()
            Platform.OS == 'ios' ? Linking.openURL('itms-apps://itunes.apple.com/us/app/apple-store/1597884858?mt=8') :
                Linking.openURL('market://details?id=com.intlabs.intface')
        } catch (err) {
            console.log('goToUpdate error', err)
        }
    }

    const showDialogUpdate = () => {
        Alert.alert(
            "Thông báo",
            `Đã có phiên bản mới nhất ${Platform.OS == 'android' ? detailVersionNew?.android?.version_name : detailVersionNew?.ios?.version_name}, bạn có muốn cập nhật không?`,
            [
                {
                    text: "Huỷ",
                    style: "cancel"
                },
                {
                    text: "Đồng ý",
                    onPress: goToUpdate,
                },
            ]
        );
    }

    if (!isShowNewVersion) return <View />

    return (
        <TouchableOpacity
            onPress={showDialogUpdate}
            activeOpacity={0.9}
            style={{
                position: 'absolute',
                zIndex: 999,
                backgroundColor: theme.mainColor,
                bottom: RFValue(76),
                right: RFValue(12),
                borderRadius: RFValue(4),
                shadowColor: ListColorsSystem.black,
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,
                elevation: 2,
            }}>
            <View style={{
                alignItems: 'center',
                width: RFValue(62),
                padding: RFValue(10),
            }}>
                <FastImage
                    style={{
                        width: RFValue(42),
                        height: RFValue(42),
                    }}
                    source={IC_UPDATE} />

                <Text style={{
                    fontSize: RFValue(8),
                    color: ListColorsSystem.white,
                    textAlign: 'center',
                    marginTop: RFValue(4),
                    fontFamily: FONT_SEMIBOLD,
                }}>Có phiên bản mới</Text>

                <TouchableOpacity
                    onPress={closeNewVersion}
                    style={{
                        width: RFValue(16),
                        height: RFValue(16),
                        borderRadius: RFValue(8),
                        position: 'absolute',
                        backgroundColor: ListColorsSystem.red30,
                        top: RFValue(-4),
                        right: RFValue(-4),
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    activeOpacity={0.8}>
                    <Text style={{
                        fontSize: RFValue(11),
                        fontFamily: FONT_BOLD,
                        color: ListColorsSystem.white,
                    }}>x</Text>
                </TouchableOpacity>

            </View>
        </TouchableOpacity>
    )
}

export default ViewCheckNewVersion