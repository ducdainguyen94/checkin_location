import { CommonActions, StackActions } from '@react-navigation/native';
let _navigator = null;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

const back = () => {
  console.log('_navigator_navigator', _navigator)
  _navigator.current.dispatch(CommonActions.goBack());
};

function navigate(routeName, params, isId = true) {
  _navigator.current.dispatch(
    CommonActions.navigate({
      name: routeName,
      params: isId ? {
        id: params,
      } : params,
    }),
  );
}

function navigateOnNotification(notifi) {
  
}

function replace(routeName) {
  _navigator.current.dispatch(
    StackActions.replace(routeName),
  );
}

// add other navigation functions that you need and export the
export default {
  navigate,
  setTopLevelNavigator,
  back,
  navigateOnNotification,
  replace
};
