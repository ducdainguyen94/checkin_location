import React, {
  createContext,
  useMemo,
  useEffect,
  useState,
} from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SplashScreen from 'react-native-splash-screen';
import NavigationService from '../navigation/NavigationService';

//Screen
import CheckInScreen2 from '../screens/CheckIn/CheckInScreen';
import SettingScreen from '../screens/Setting';

//redux
//actions

export const AuthContext = createContext();
const RootStack = createStackNavigator();
const TabStack = createBottomTabNavigator();
const CheckStack = createStackNavigator();
const AppStack = createStackNavigator();
import TabBarCustom from '../components/TabBarCustom';

import { RFValue } from '../constants/multiScreen';

import ListColorsSystem from '../themes/ListColorsSystem';
import DashboardScreen from '../screens/DashboardScreen/DashboardScreen';
import { Text, View, Platform, DeviceEventEmitter } from 'react-native';


function TabBottom() {
  return (
    <TabStack.Navigator
      initialRouteName="CheckInScreen"

      screenOptions={({ route }) => ({
        tabBarLabel: ({ focused, color, size, tabBarLabel }) => {
          const { name } = route;
          return null;
        },
        tabBarIcon: ({ focused, color, size }) => {
          const { name } = route;
          return (
            <TabBarCustom focused={focused} isIcon={true} routeName={name}   // name chính là route của screenOptions
             />
          );
        },
      })}


      tabBarOptions={{  
        activeTintColor: ListColorsSystem.mainColor,
        inactiveTintColor: ListColorsSystem.grey40,
        style: {     // style for all tabbar
          paddingTop: Platform.isPad ?RFValue(2) : RFValue(10),
          marginBottom:RFValue(5)
        },
      }}
    >
   

      <TabStack.Screen
        name="DashboardScreen"
        component={DashboardScreen}
      />

      <TabStack.Screen
        name="CheckInScreen"
        component={CheckInScreen2}
      />

      <TabStack.Screen
        name="Setting"
        component={SettingScreen}
      />
    </TabStack.Navigator>
  );
}

const AppStackScreen = () => (
  <AppStack.Navigator
    screenOptions={{
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}
    headerMode={'none'}>
    <CheckStack.Screen name="HomeTab" component={TabBottom} path={'home'} />
    <CheckStack.Screen name="CheckIn" component={CheckInScreen2} path={'/checkin'} />
  </AppStack.Navigator>
);

const RootStackScreen = () => {
  SplashScreen.hide();
  return (
    <RootStack.Navigator headerMode="none">
      <RootStack.Screen name="App" component={AppStackScreen} />
    </RootStack.Navigator>
  );
};

export default () => {
  const navigationRef = React.useRef(null);


  useEffect(() => {
    NavigationService.setTopLevelNavigator(navigationRef);
  }, []);


  return (
      <NavigationContainer ref={navigationRef} >
        <RootStackScreen />
      </NavigationContainer>
  );
};

// npx uri-scheme open intface://intface.com/checkinscreen/sdhajdhkashdkahkdahsiSOkw07Jw2Wh --android
