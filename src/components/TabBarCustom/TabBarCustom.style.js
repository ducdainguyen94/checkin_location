import { StyleSheet , Platform,} from 'react-native';
import { Colors } from '../../themes';
import { FONT_SCALE, RFValue } from '../../constants/multiScreen';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    // height: Platform.isPad ? 10 : null, 
    width: Platform.isPad ? RFValue(90) : RFValue(80),
    
  },
  icon: {
    width: RFValue(20),
    height: RFValue(20),
    // marginBottom: RFValue(2),
  },
  routeName: {
    fontSize: 12,
    color: Colors.mainColor,
  },
});
