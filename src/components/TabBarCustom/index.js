import React, { useEffect, useRef } from 'react';
import { View, Text, Image, Animated, } from 'react-native';
import { IC_HOME, IC_WORKSPACE, IC_PLAY, IC_MENU_2, IC_DASHBOARD, IC_INTFACE_2, FONT_REGULAR } from '../../assets';
import styles from './TabBarCustom.style';
import { RFValue } from '../../constants/multiScreen';
import ListColorsSystem from '../../themes/ListColorsSystem';
import FastImage from 'react-native-fast-image';
import { useSelector } from 'react-redux';

const AnimFastImage = Animated.createAnimatedComponent(FastImage)

const TabBarCustom = ({
  routeName,
  focused,
  isIcon,
}) => {
  const theme = useSelector(state => state.theme)

  const _checkRouteName = (routeName) => {
    switch (routeName) {
      case 'DashboardScreen':
        return { icon: IC_DASHBOARD };
      case 'CheckInScreen':
        return { icon: IC_HOME, };
      case 'Setting':
        return { icon: IC_MENU_2 };
      default:
        return { icon: IC_PLAY };
    }
  };

  const data = _checkRouteName(routeName) || {
    routeName: 'Camera',
    icon: IC_PLAY,
  };

  const _renderIcon = () => {
   
    return (
      <Image
        resizeMode="contain"
        source={data.icon}
        style={[styles.icon,
        { tintColor: focused ? theme.mainColor : ListColorsSystem.grey40 }]}
      />
    )
  };

  const renderRouterName = () => {
    let nameLabel = ''
    switch (routeName) {
      case 'DashboardScreen':
        nameLabel = 'Lịch sử'
        break;
      case 'CheckInScreen':
        nameLabel = 'Chấm công'

        break;
      case 'Setting':
        nameLabel = 'Cài đặt'
        break;
    }

    return (
      <Text style={{
        fontSize: RFValue(focused ? RFValue(9) : RFValue(8)),
        color: focused ? theme.mainColor : ListColorsSystem.grey40,
      }}>{nameLabel}</Text>
    )
  }

  return (
    <View style={styles.container}>
      {isIcon && _renderIcon()}
      {
        renderRouterName()
      }
    </View>
  );
};

export default TabBarCustom;
