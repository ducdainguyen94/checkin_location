import React, { memo, useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import { RFValue } from '../../constants/multiScreen';

const HeaderBarHome = ({title}) => {
    const theme = useSelector(state => state.theme)

    return (
        <View style={{
            width: '100%',
            backgroundColor: theme.mainColor,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.30,
            shadowRadius: 1.41,
            elevation: 2,
        }}>
            <SafeAreaView>
                <View style={{
                    paddingVertical: RFValue(10),
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: RFValue(12),
                    justifyContent: 'center'
                }}>
                   <Text style={{
                    color:'white',
                    fontSize:RFValue(16)
                   }}>{title ?? ''}</Text>
                </View>
            </SafeAreaView>
        </View>
    )
}

export default memo(HeaderBarHome);