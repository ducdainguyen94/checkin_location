const Colors = {
  mainColor: '#209CFF',
  primary: '#76b4bd',
  transparent: 'transparent',
  white: 'white',
  white10: 'rgba(255, 255, 255, 0.1)',
  white20: 'rgba(255, 255, 255, 0.2)',
  white30: 'rgba(255, 255, 255, 0.3)',
  white50: 'rgba(255, 255, 255, 0.5)',
  black10: 'rgba(0, 0, 0, 0.1)',
  black30: 'rgba(0, 0, 0, 0.3)',
  black20: 'rgba(0, 0, 0, 0.2)',
  black50: 'rgba(0, 0, 0, 0.5)',
  blue: '#C3BFF9',
  blue2: '#5D58A6',
  oranges: '#964915',
  green: '#918704',
  colorBoder: '#DEDDDF',
  overlayDarkBlue2: 'rgba(26, 31, 58, .8)',
  btnModalOk: '#3EA4D3',
  btnModalCancel: '#F4648B',
};

export default Colors;
