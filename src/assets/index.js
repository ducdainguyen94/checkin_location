//image
export const IMG_BACKGROUND = require('./images/img_background.png');
export const IMG_FRAME_SCAN = require('./images/framesScan.png');
export const IMG_EVENT = require('./images/img_event.png')

// intlabs
export const IM_INTLABS = require('./images/img_intlabs.png')

//icon
export const IC_HOME = require('./icons/ic_home.png');
export const IC_INTFACE_2 = require('./icons/ic_intface_2.png');
export const IC_DASHBOARD = require('./icons/ic_dashboard.png');
export const IC_WORKSPACE = require('./icons/ic_workspace.png');
export const IC_CALENDAR = require('./icons/ic_calendar.png');
export const IC_NOTIFI = require('./icons/ic_notifi.png');
export const IC_MENU_2 = require('./icons/ic_menu_2.png');
export const IC_OUT = require('./images/ic_logout.png');
export const IC_CAKE = require('./icons/ic_cake.png');
export const IC_HONORS = require('./icons/ic_badge.png');
export const IC_USER = require('./icons/ic_user.jpeg');

export const IC_DOCUMENT = require('./images/ic_document.png');

export const IC_QR_CI = require('./images/ic_qr_ci.png');
export const IC_FACE_CI = require('./images/ic_face_ci.png');
export const IC_WIFI_CI = require('./images/ic_wifi_ci.png');
export const IC_LOCATION_CI = require('./images/ic_location_ci.png');

//setting
export const IC_CHAT = require('./images/ic_chat.png')
export const IC_CHANGEPASS = require('./images/padlock.png')
export const IC_CHECKUPDATE = require('./images/ic_checkupdate.png')
export const IC_POLICY = require('./images/ic_policy.png')
export const IC_TERMS = require('./images/ic_terms.png')
// empty
export const IMG_EMPTY_FILE = require('./images/img_empty_files.png')
export const IMG_EMPTY_IMAGE = require('./images/img_empty_image.png')
export const IMG_EMPTY_VIDEO = require('./images/img_empty_video.png')

export const IMG_THUMBNAIL_VIDEO = require('./images/thumbnail-video.png')
export const IC_UPDATE = require('./icons/ic_update.png')

// Font text
export const FONT_BOLD = 'OpenSans-Bold'
export const FONT_BOLD_ITALIC = 'OpenSans-BoldItalic'
export const FONT_EXTRABOLD = 'OpenSans-ExtraBold'
export const FONT_EXTRABOLD_ITALIC = 'OpenSans-ExtraBoldItalic'
export const FONT_ITALIC = 'OpenSans-Italic'
export const FONT_LIGHT = 'OpenSans-Light'
export const FONT_LIGHT_ITALIC = 'OpenSans-LightItalic'
export const FONT_REGULAR = 'OpenSans-Regular'
export const FONT_SEMIBOLD = 'OpenSans-SemiBold'
export const FONT_SEMIBOLD_ITALIC = 'OpenSans-SemiBoldItalic'


// webp
export const IC_CANHAN = require('./webp/ic_canhan.png')
export const IC_DON = require('./webp/ic_don.png')
export const IC_DUYET_DON = require('./webp/ic_duyet_don.png')
export const IC_HOP = require('./webp/ic_hop.png')
export const IC_KHIEUNAI = require('./webp/ic_khieunai.png')
export const IC_DUYET_KHIEUNAI = require('./webp/ic_duyet_khieunai.png')
export const IC_LICH = require('./webp/ic_lich.png')
export const IC_OT = require('./webp/ic_ot.png')
export const IC_DUYET_OT = require('./webp/ic_duyet_ot.png')
export const IC_THAMNIEN = require('./webp/ic_thamnien.png')
export const IC_CAM = require('./webp/ic_cam.png')
export const IC_ADD_HOP = require('./webp/ic_add_hop.png')
export const IC_ADD = require('./webp/ic_add.png')
export const IC_DANH_BA = require('./webp/ic_danhba.png')
export const IC_NOTI = require('./webp/ic_noti.png')
