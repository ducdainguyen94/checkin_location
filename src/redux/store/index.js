import { createStore, applyMiddleware } from 'redux';
import reducers from '../reducers';
import rootSaga from '../../sagas/rootSaga';
import ReduxPersist from '../../config/ReduxConfig';
import { persistStore, persistReducer } from 'redux-persist';
const persistedReducer = persistReducer(ReduxPersist.storeConfig, reducers);
//redux saga
import createSagaMiddleware from 'redux-saga';
//Middleware
const sagaMiddleware = createSagaMiddleware();

const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
const persistor = persistStore(store);
sagaMiddleware.run(rootSaga);
export { store, persistor };
