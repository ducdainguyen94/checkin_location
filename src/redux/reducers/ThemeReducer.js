import { ListTheme } from '../../themes/ListTheme';
import Types from '../types';

const ThemeReducer = (state = ListTheme[0], action) => {
  const { type, payload } = action;
  switch (type) {
    case Types.CHANGE_THEME_SYSTEM:
      let tmp = payload;
      if (payload >= ListTheme.length) {
        tmp = ListTheme.length - 1;
      }
      return ListTheme[tmp]
    default:
      return state;
  }
};

export default ThemeReducer;
