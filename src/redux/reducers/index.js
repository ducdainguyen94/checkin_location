import { combineReducers } from 'redux';
import ThemeReducer from './ThemeReducer';
import Location from './Location';

const reducers = combineReducers({
  theme: ThemeReducer,
  location:Location
});

export default reducers;
