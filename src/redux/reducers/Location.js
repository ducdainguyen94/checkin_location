import Types from '../types';

const initial = {
   lat:"21.031619311502627",
   lng:"105.78168135694642",
   radius:100
}

const Location = (state = initial, action) => {
    const { type, payload } = action;
    console.log('actionactionaaa',action)
    switch (type) {
        case Types.SET_LOCATION:
            return {
                ...state,
                lat:payload?.lat,
                lng:payload?.lng
            }
        case Types.SET_RADIUS:
            return {
                ...state,
                radius:payload
            }
        default:
            return state;
    }
};

export default Location;
