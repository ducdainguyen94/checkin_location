import Types from '../types';

export const changeThemeSystem = (theme) => {
  return {
    type: Types.CHANGE_THEME_SYSTEM,
    payload: theme
  };
};
