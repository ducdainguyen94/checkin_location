import React, { useEffect, useCallback, useState, useRef } from 'react'
import { View, Text, StyleSheet, ScrollView, ActivityIndicator, TouchableOpacity, RefreshControl, FlatList, Image } from 'react-native'
import HeaderBarHome from '../../components/HeaderBarHome/HeaderBarHome'
import { useFocusEffect } from '@react-navigation/native';
import ListColorsSystem from '../../themes/ListColorsSystem'
import FastImage from 'react-native-fast-image';
import { useDispatch,useSelector } from 'react-redux'
import storage,{firebase} from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import { RFValue } from '../../constants/multiScreen';
import moment from 'moment';
const DashboardScreen = () => {
    const defaultStorageBucket = storage();
    const secondaryStorageBucket = firebase.app().storage('gs://checkin-location-16f41.appspot.com');
    const dispatch = useDispatch();
    const theme = useSelector(state => state.theme)
    const reference = storage().ref('images');
    const refFlatlist = useRef()
    const [isRefresh,setIsRefresh] = useState(false);
    const [listItem,setListItem] = useState([]);
    const [showDetail,setShowDetail] = useState(null)
    useEffect(() => {
        console.log('vao')
        getInfo();
    }, [])
//     useFocusEffect(
//     React.useCallback(() => {
//         getInfo();
//     }, [])
// );
    const getImage = async (data) => {
        try {
        console.log('data',data)

        const result = await reference.list()
        console.log('result',result)

            result.items.forEach(ref => {
              ref.getDownloadURL().then((url) =>{
               const newList = data.map(item => {
                    if(item.id == ref.fullPath){
                        item.urlImage = url
                    }
                    return {
                        ...item
                    }
                })
                console.log('newListnewList',newList)
            
            newList.sort((a,b) => b.time - a.time)
            console.log('newListnewList2',newList)
            setIsRefresh(false)
               setListItem(newList)
              })
            })
        } catch (error) {
            console.log('errorerror',error)
            setIsRefresh(false)
        }
        
    }
   const getInfo = async () => {
    try {
        
        setIsRefresh(true)
        const data = await firestore().collection('detail_checkin').get()
        let dataTmp = [];
        console.log('data',data)
        data.forEach(item => {
            dataTmp.push(item.data())
        })
        getImage(dataTmp)
    } catch (error) {
        setIsRefresh(false)
        console.log('errorerrorerror',error)
    }
   
   }

   const renderItem = (item) => {
    return(
        <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => {
            setShowDetail(item.id)
        }}
        style={{
            justifyContent:'space-between',
            alignItems:'center',
            marginTop:RFValue(15)
        }}>
            <FastImage
            
                source={{uri:item.urlImage}}
                resizeMode={FastImage.resizeMode.contain}
                style={{ width: RFValue(150), height: RFValue(150) }}
            />
           {item.id == showDetail && <View
            style={{
                marginTop:RFValue(10)
            }}
           >
                <Text>
                    {moment.unix(item.time).format('hh:mm DD/MM/YYYY')}
                </Text>
            </View>}
        </TouchableOpacity>
    );
   }
   const onRefresh = () => {
    getInfo()
   }
       const renderEmptyList = () => {
        return (
            <View style={{
                justifyContent: 'center',
                paddingVertical: RFValue(50)
            }}>
                <Text>Không có dữ liệu</Text>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <HeaderBarHome title={'Lịch sử'} />
            <FlatList
            horizontal={false}
            ref={refFlatlist}
            initialNumToRender={100}
            numColumns={2}
                refreshControl={
                    <RefreshControl refreshing={isRefresh} onRefresh={onRefresh} />
                  }
                  ListEmptyComponent={renderEmptyList}
                data={listItem}
                keyExtractor={(item,index) => `key_${index}`}
                renderItem={({item}) => renderItem(item)}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ListColorsSystem.backgroundColor,
        justifyContent:'center',
        alignItems:'center'
    },
})

export default DashboardScreen