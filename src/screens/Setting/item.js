
import React, {  useState, useEffect, memo } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  Switch,
  ActivityIndicator
} from 'react-native';
//components
import { RFValue } from '../../constants/multiScreen';
import {
  FONT_REGULAR,

} from '../../assets/index';

import ListColorsSystem from '../../themes/ListColorsSystem';
//styles
import styles from './Setting.style';


export const Item = memo(({
    icon,
    title,
    onPress,
    line,
    showNotification,
    toggleSwitch,
    notification,
    colorIcon,
    isLoading = false,
    colorLoading = ListColorsSystem.mainColor,
    renderRight,
  }) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.8}
        style={{ marginLeft: RFValue(20) }}>
        <View style={[styles.viewItem, { marginBottom: line ? RFValue(10) : RFValue(0) }]}>
          <View style={styles.viewItem2}>
            <Image
              resizeMode="contain"
              style={{
                tintColor: colorIcon,
                width: RFValue(25),
                height: RFValue(25),
              }}
              source={icon}
            />
            <Text style={{
              paddingLeft: RFValue(20),
              fontSize: RFValue(11),
              fontFamily: FONT_REGULAR
            }}>{title}</Text>
          </View>
  
          {notification && <Switch
            style={{ transform: [{ scale: 0.8 }], }}
            trackColor={{ false: "#959595", true: colorIcon }}
            thumbColor={showNotification ? "white" : "white"}
            ios_backgroundColor="#959595"
            onValueChange={toggleSwitch}
            value={showNotification}
          />}
          {
            isLoading && (
              <ActivityIndicator
                style={{
                  marginHorizontal: RFValue(18)
                }}
                color={colorLoading} />
            )
          }
          {
            renderRight && renderRight()
          }
        </View>
        {!line && <View style={{
          height: 0.5,
          marginLeft: RFValue(40),
          backgroundColor: ListColorsSystem.grey60,
          marginTop: RFValue(10)
        }} />}
      </TouchableOpacity>
    )
  })