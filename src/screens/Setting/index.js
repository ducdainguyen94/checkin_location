import React, {  useState, useEffect, memo } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Alert,
  Image,
  Platform,
  Linking,
  ScrollView,
  Switch,
  ActivityIndicator,
  TextInput
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthContext } from '../../navigation';
import { useSelector, useDispatch } from 'react-redux';

//components
import { RFValue } from '../../constants/multiScreen';
import {
  IC_CHECKUPDATE,
} from '../../assets/index';
import { Item } from './item';
//acitons
import { setLocation,setRadius } from '../../redux/actions/Location';
//apis
import ListColorsSystem from '../../themes/ListColorsSystem';
//styles
import styles from './Setting.style';
import HeaderBarHome from '../../components/HeaderBarHome/HeaderBarHome';

const SettingScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const theme = useSelector(state => state.theme)
  const state = useSelector(state => state)
  console.log('statestatestatestatestate',state?.location?.radius)
  const [showLocation,setShowLocation] = useState(false)
  const [showRadius,setShowRadius] = useState(false)
  const [stateRadius,setStateRadius] = useState(state?.location?.radius)
  const [stateLocation,setStateLocation] = useState({lat:state?.location?.lat,lng:state?.location?.lng})

  const actionSetRadius = () => {
    setShowRadius(true)
    setShowLocation(false)
  }

  const actionSetLocation = () => {
    setShowLocation(true)
    setShowRadius(false)
  }

  const renderRadius = () => {
    return(
      <View style={{
        marginLeft:RFValue(35),
        marginRight:RFValue(15),

      }}>
        <View style={{
          flexDirection:'row',
          alignItems:'center',
          marginTop:RFValue(5),
        }}>
          <Text style={{
            fontSize:RFValue(11),
          }}>Bán kính (m)</Text>
        <TextInput
        onFocus={()=>setStateRadius('')}
        placeholder='Nhập bán kính'
        value={`${stateRadius}`}
        style={{
          fontSize:RFValue(11),
          marginLeft:RFValue(10),
          borderRadius:RFValue(5),
          borderWidth:0.5,
          paddingHorizontal:RFValue(12),
          paddingVertical:RFValue(12),
          flex:1
        }}
          onChangeText={(text) => setStateRadius(text)}
      />
      <TouchableOpacity
        onPress={()=>{
          dispatch(setRadius(stateRadius))
          Alert.alert('Thông báo', 'Lưu thông tin thành công')
        }}
          style={{
            // flex:2,
            marginTop:RFValue(5),
            borderRadius:RFValue(10),
            // alignItems:'center',
            marginLeft:RFValue(10),
            alignSelf:'flex-end',
            backgroundColor:theme.mainColor,
            paddingVertical:RFValue(10),
            paddingHorizontal:RFValue(20)

          }}
        >
          <Text style={{color:'white',fontSize:RFValue(13)}}>Lưu</Text>
        </TouchableOpacity>
        </View>
        
      </View>
      
    )
  }

  const renderLocation = () => {
    return(
      <View style={{
        marginLeft:RFValue(35),
        marginRight:RFValue(15),
        
      }}>
        <View style={{
          flexDirection:'row',
          alignItems:'center',
         marginTop:RFValue(5)
        }}>
          <Text style={{
            flex:1,
            fontSize:RFValue(11),
          }}>Latitude</Text>
        <TextInput
        placeholder='Nhập toạ độ'
        onFocus={()=>setStateLocation({
          lat:'',
          lng:stateLocation.lng
        })}
        value={`${stateLocation.lat ?? '0.000'}`}
        style={{
          fontSize:RFValue(11),
          borderRadius:RFValue(5),
          borderWidth:0.5,
          paddingHorizontal:RFValue(12),
          paddingVertical:RFValue(12),
          flex:3
        }}
          onChangeText={(text) => setStateLocation({
            lat:text,
            lng:stateLocation?.lng
          })}
      />
        </View>

        <View style={{
          flexDirection:'row',
          alignItems:'center',
          marginTop:RFValue(10)
        }}>
          <Text style={{
            // paddingHorizontal:RFValue(20),
            fontSize:RFValue(11),
            flex:1
          }}>Longitude</Text>
        <TextInput
        onFocus={()=>setStateLocation({
          lat:stateLocation.lat,
          lng:''
        })}
        value={`${stateLocation.lng ?? '0.000'}`}
        placeholder='Nhập toạ độ'
        style={{
          fontSize:RFValue(11),
          borderRadius:RFValue(5),
          borderWidth:0.5,
          paddingHorizontal:RFValue(12),
          paddingVertical:RFValue(12),
          flex:3
        }}
          onChangeText={(text) => setStateLocation({
            lat:stateLocation?.lat,
            lng:text
          })}
      />
        </View>
        <TouchableOpacity
        onPress={()=> {
          dispatch(setLocation(stateLocation))
          Alert.alert('Thông báo', 'Lưu thông tin thành công')
        }}
          style={{
            marginTop:RFValue(5),
            marginBottom:RFValue(5),
            borderRadius:RFValue(10),
            // alignItems:'center',
            alignSelf:'flex-end',
            backgroundColor:theme.mainColor,
            paddingVertical:RFValue(10),
            paddingHorizontal:RFValue(20)

          }}
        >
          <Text style={{color:'white',fontSize:RFValue(13)}}>Lưu</Text>
        </TouchableOpacity>
      </View>
      
    )
  }

  return (
    <View style={{
      flex: 1,
      backgroundColor: ListColorsSystem.backgroundColor,
    }}>
      <ScrollView 
      scrollEnabled={false}
      keyboardShouldPersistTaps='handled'>
      <HeaderBarHome title={'Cài đặt'} />
        <Text style={[styles.textTitle, {
          color: theme.mainColorDark,
        }]}>Cài đặt</Text>
        <View style={styles.viewManager}>
          <Item icon={IC_CHECKUPDATE}
          renderRight={() => (
            <Text style={{
              marginRight: RFValue(12),
              fontSize: RFValue(11),
              color: ListColorsSystem.grey30,
            }}>{state?.location?.radius} m </Text>
          )}
          colorIcon={ListColorsSystem.deep_orange20}
          onPress={actionSetRadius} title={'Cài đặt bán kính'} />
          {showRadius && renderRadius()}
          <Item onPress={actionSetLocation}
            title={'Cài đặt vị trí'}
            colorIcon={ListColorsSystem.light_blue10}
            icon={IC_CHECKUPDATE} />
            {showLocation && renderLocation()}
        </View>

        <View style={{ height: RFValue(20) }} />
      </ScrollView>
      
    </View>
  );
};

export default SettingScreen;
