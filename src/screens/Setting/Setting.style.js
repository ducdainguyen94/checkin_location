import { StyleSheet, Dimensions } from 'react-native';
import { FONT_BOLD, FONT_SEMIBOLD } from '../../assets';
import { RFValue } from '../../constants/multiScreen';
import ListColorsSystem from '../../themes/ListColorsSystem';

const { height } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  viewItem: {
    flexDirection: 'row',
    marginTop: RFValue(10),
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewItem2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewManager: {
    flex:1,
    alignSelf: 'center',
    marginTop: RFValue(4),
    width: '100%',
    borderRadius: 5,
    backgroundColor: ListColorsSystem.white,
    shadowColor: ListColorsSystem.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.30,
    shadowRadius: 0.71,
    elevation: 1,
  },
  viewAcount: {
    padding: RFValue(8),
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: RFValue(4),
  },
  viewManager2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: "100%",
    paddingVertical: RFValue(20),
    borderRadius: 5,
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
    paddingHorizontal: RFValue(10)
  },
  txtName: {
    fontWeight: '700',
    fontSize: RFValue(14)
  },
  txtNameAcc: {
    paddingBottom: RFValue(5),
    fontSize: RFValue(12),
    fontWeight: '700'
  },
  txtManager: {
    fontWeight: '700',
    paddingLeft: RFValue(5),
    fontSize: RFValue(14)
  },
  viewManagerDetail: {
    flexDirection: 'row', alignItems: 'center'
  },
  viewIcon: {
    width: RFValue(52),
    height: RFValue(52),
    borderRadius: RFValue(26),
    alignItems: 'center',
    justifyContent: 'center'
  },
  txtIcon: {
    textAlign: 'center',
    fontSize: RFValue(28),
    color: 'white'
  },
  containerViewCompany: {
    backgroundColor: ListColorsSystem.white,
    marginHorizontal: RFValue(12),
    marginTop: RFValue(10),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    padding: RFValue(14),
    borderRadius: RFValue(6),
    alignItems: 'center',
  },
  viewBoundCompany: {
    width: RFValue(60),
    height: RFValue(60),
    borderRadius: RFValue(60),
    backgroundColor: ListColorsSystem.white,
    shadowColor: ListColorsSystem.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 0.71,
    elevation: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: RFValue(12),
  },
  imageCompany: {
    width: RFValue(54),
    height: RFValue(54),
    borderRadius: RFValue(27),
  },
  textCompany: {
    fontSize: RFValue(13),
    fontFamily: FONT_BOLD,
    flex: 1,
  },
  textTitle: {
    paddingHorizontal: RFValue(12),
    fontSize: RFValue(14),
    fontFamily: FONT_SEMIBOLD,
    marginTop: RFValue(12)
  },
  viewHorizontalCompany: {
    flexDirection: 'row',
    marginVertical: RFValue(12),
    alignItems: 'center'
  }
});
