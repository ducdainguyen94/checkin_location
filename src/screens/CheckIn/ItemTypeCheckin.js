import React, { useEffect, useRef } from 'react'
import { TouchableOpacity, Text, } from 'react-native'
import { FONT_SEMIBOLD } from '../../assets'
import { RFValue } from '../../constants/multiScreen'
import ListColorsSystem from '../../themes/ListColorsSystem';
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image';

const ItemTypeCheckin = ({
    icon,
    title,
    onPress,
    colorBackground = [
        ListColorsSystem.white,
        ListColorsSystem.mainColorLight4,
    ],
}) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
                if (onPress) {
                    onPress()
                }
            }}
            style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,
                elevation: 2,
                borderRadius: RFValue(4)
            }}>
            <LinearGradient
                start={{ x: 0.2, y: 0 }}
                end={{ x: 0.8, y: 0.6 }}
                style={{
                    padding: RFValue(5),
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '100%',
                    borderRadius: RFValue(4),
                }}
                colors={colorBackground}
                speed={15000}
            >
                <FastImage style={{
                    width: RFValue(40),
                    height: RFValue(40),
                }}
                    resizeMode={'contain'}
                    source={icon}
                />

                <Text style={{
                    fontSize: RFValue(10),
                    fontFamily: FONT_SEMIBOLD,
                    marginTop: RFValue(10),
                }}>{title}</Text>
            </LinearGradient>
        </TouchableOpacity >
    )
}

export default ItemTypeCheckin