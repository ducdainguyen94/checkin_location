import React, { memo, useEffect, useRef, useState } from 'react';
import {
    View,
    Text,
    Platform,
    Dimensions,
    ActivityIndicator,
    Alert,
    TouchableOpacity,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import MapView, { PROVIDER_GOOGLE, Marker, Circle } from 'react-native-maps';
import ImagePicker from 'react-native-image-crop-picker';
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
//components
import {
    IC_LOCATION_CI,
} from '../../assets';
// import CheckBox from './CheckBox';
import { RFValue } from '../../constants/multiScreen';
//styles
import styles from './CheckIn.style';
import ListColorsSystem from '../../themes/ListColorsSystem';
import HeaderBarHome from '../../components/HeaderBarHome/HeaderBarHome';
import ItemTypeCheckin from './ItemTypeCheckin';

const { width,height } = Dimensions.get('screen')

const CheckInScreen = ({
    navigation,
    route
}) => {
    const currentPosition = useRef()
    const locationApp = useSelector(state => state.location) 
    const branchPosition = {
        latitude:parseFloat(locationApp?.lat) ,
        longitude:parseFloat(locationApp?.lng) 
    }
    const [loading, setLoading] = useState(false)
    const [valueLoading, setValueLoading] = useState('Đang gửi thông tin chấm công...')

    const [locationBranch, setLocationBranch] = useState()
    const [isShowInfo, setIsShowInfo] = useState(false)

    const refMapBox = useRef()
    const closeInfoCheckIn = () => setIsShowInfo(false)

    const theme = useSelector(state => state.theme)

    const isFirstMoveToCurrent = useRef(false)

    const dispatch = useDispatch()

    const getDistance = (latP1, lonP1, latP2, lonP2) => {
        // Convert the coordinates to radians
        let lat1 = latP1 * (Math.PI / 180);
        let lon1 = lonP1 * (Math.PI / 180);
        let lat2 = latP2 * (Math.PI / 180);
        let lon2 = lonP2 * (Math.PI / 180);

        // Calculate the distance between the two points
        let R = 6371; // Radius of the Earth in km
        let dLat = lat2 - lat1;
        let dLon = lon2 - lon1;
        let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1) * Math.cos(lat2) *
                Math.sin(dLon/2) * Math.sin(dLon/2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c; // Distance in km
        return d;
      };

    const onUserLocationChange = (e) => {
        currentPosition.current = { lat: e?.nativeEvent?.coordinate?.latitude, lng: e?.nativeEvent?.coordinate?.longitude, }
        if (!isFirstMoveToCurrent.current && e?.nativeEvent?.coordinate) {
            refMapBox.current.animateToRegion({
                latitude: e?.nativeEvent?.coordinate?.latitude,
                longitude: e?.nativeEvent?.coordinate?.longitude,
                latitudeDelta: 0.003,
                longitudeDelta: 0.003,
            }, 1000)
            isFirstMoveToCurrent.current = true
        }
    }

    const checkInLocation = async () => {
        try {
            if (currentPosition.current) {
               
                let lat1 = currentPosition.current?.lat
                let lng1 = currentPosition.current?.lng
                let lat2 = branchPosition.latitude
                let lng2 = branchPosition.longitude
                let distance = getDistance(lat1,lng1,lat2,lng2)
                if(distance*1000 >= parseFloat(locationApp?.radius)){
                    setLoading(false)
                    return Alert.alert('khoảng cách quá xa để chấm công')
                }
                const image = await ImagePicker.openCamera({
                    width: 300,
                    height: 400,
                    cropping: true,
                  })
                  console.log('iamammmaamam',image)
                  let imageName = image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length)
                  setLoading(true)
                  setValueLoading('Đang gửi thông tin địa điểm...')
                  console.log('imageNameimageName',imageName)
                  const reference = storage().ref(`images/${imageName}`)
                  await reference.putFile(image.path)
                  const task = await reference.putFile(image.path);
                  console.log('tasktasktasktasktask',task)
                  if(task.state == 'success'){
                    console.log('moment().unix()',moment().unix(),currentPosition.current?.lat,currentPosition.current?.lng)
                   firestore()
                        .collection('detail_checkin').doc(imageName)
                        .set({
                            id:`images/${imageName}`,
                            time: moment().unix(),
                            lat: currentPosition.current?.lat,
                            lng:currentPosition.current?.lng,
                        }).then(() =>  Alert.alert('Thông báo', 'Gửi thông tin thành công'));
                    
                  }
                setLoading(false)
            } else {
                setLoading(false)
                dispatch(ActPushToast({
                    title: 'Lỗi',
                    type: 'error',
                    message: 'Lỗi lấy thông tin vị trí',
                }))
            }
        } catch (err) {
            setLoading(false)
            dispatch(ActPushToast({
                title: 'Lỗi',
                type: 'error',
                message: err?.response?.data?.message || 'Lỗi gửi thông tin chấm công từ xa.',
            }))
            console.log('sendInfoCheckIn error', err)
        }
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: ListColorsSystem.backgroundColor,
        }}>
            <HeaderBarHome title={'Chấm công'}/>
            {
                    loading && (
                        <View style={{
                            width: width,
                            height: '100%',
                            backgroundColor: 'rgba(0, 0, 0, 0.3)',
                            position: 'absolute',
                            zIndex: 99999,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <View style={{
                                width: '70%',
                                padding: RFValue(20),
                                backgroundColor: '#fff',
                                borderRadius: RFValue(4),
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <ActivityIndicator color={theme.mainColor} />
                                <Text style={{
                                    marginTop: RFValue(12),
                                    fontSize: RFValue(10),
                                }}>{valueLoading}</Text>
                            </View>
                        </View>
                    )
                }
            <View style={styles.container}>
                <View
                    // showsVerticalScrollIndicator={false}
                    style={{
                        flex: 1,
                        width: '100%',
                    }}>
                    <View style={{
                        marginTop: RFValue(10),
                        marginHorizontal: RFValue(12),
                    }}>
                        <Text style={{
                            fontSize: RFValue(9),
                            color: theme.mainColorLight,
                            marginBottom: RFValue(4)
                        }}>Thông tin kết nối</Text>
                        <View style={{
                            paddingHorizontal: RFValue(12),
                            paddingVertical: RFValue(8),
                            borderRadius: 4,
                            width: '100%',
                            flexDirection: 'row',
                            borderColor: theme.mainColorLight2,
                            alignItems: 'center',
                            backgroundColor: ListColorsSystem.white,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.20,
                            shadowRadius: 1.41,
                            elevation: 2,
                        }}>
                            <View style={{ flex: 1 }}>
                                <View style={{
                                    height: height * 0.52,
                                    marginTop: RFValue(4),
                                    borderRadius: RFValue(4),
                                    overflow: 'hidden',
                                }}>
                                    <MapView
                                        provider={PROVIDER_GOOGLE}
                                        style={{ flex: 1 }}
                                        ref={refMapBox}
                                        showsUserLocation
                                        followsUserLocation
                                        onUserLocationChange={onUserLocationChange}
                                    >
                                        <Circle
                                            center= {branchPosition}
                                            radius = {RFValue(locationApp?.radius)}
                                            fillColor = "rgba(255, 0, 0, 0.2)"
                                            // strokeWidth = "rgba(255, 0, 0, 0.2)"
                                        >
                                            <Marker
                                                    key={'index2'}
                                                    coordinate={branchPosition}
                                                />
                                        </Circle>
                                        {
                                            locationBranch && (
                                                <Marker
                                                    key={'index'}
                                                    coordinate={locationBranch}
                                                />
                                            )
                                        }
                                    </MapView>
                                </View>
                            </View>
                        </View>
                    </View>

            
                </View>

                <View style={{
                            borderRadius: 4,
                            borderWidth: 1,
                            // width: '30%',
                            marginHorizontal:RFValue(15),
                            flexDirection: 'row',
                            borderColor: theme.mainColorLight2,
                            justifyContent: 'center',
                            marginBottom:RFValue(20)
                        }}>
                            <ItemTypeCheckin
                                    icon={IC_LOCATION_CI}
                                    title={'Chấm công'}
                                    sizeView={RFValue(40)}
                                    onPress={checkInLocation}
                                    colorBackground={[ListColorsSystem.light_green70,
                                    ListColorsSystem.light_green90]}
                                />
                        </View>
            </View>

        </View>
    )
}

export default CheckInScreen