import { StyleSheet, Dimensions, Platform } from 'react-native';
import { FONT_REGULAR, FONT_SEMIBOLD } from '../../assets';
import { FONT_SCALE, isIphoneX, RFValue } from '../../constants/multiScreen';
import ListColorsSystem from '../../themes/ListColorsSystem';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent:'center',
    width: '100%',
    flex: 1,
  },

  viewHori: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: RFValue(3),
  },
 
});
