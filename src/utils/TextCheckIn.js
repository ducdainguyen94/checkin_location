import ListColorsSystem from "../themes/ListColorsSystem"

export const getTextCheckIn = (timeStart, timeEnd, checkIn, checkOut) => {
    if (checkIn == undefined || checkIn == '') {
        return { text: 'Chưa vào/ra', background: ListColorsSystem.black20 }
    }
    if (checkOut != undefined) {
        if (timeEnd == undefined) {
            return { text: `Đã Check Out`, background: ListColorsSystem.amber30 }
        } else {
            if (checkOut < timeEnd) {
                return { text: `Về sớm`, background: ListColorsSystem.red50 }
            } else {
                return { text: `Đã Check Out`, background: ListColorsSystem.green40 }
            }
        }
    } else {
        if (timeStart == undefined) {
            return { text: 'Đã Check In', background: ListColorsSystem.purple40 }
        } else {
            if (timeStart < checkIn) {
                let tmp = checkIn - timeStart
                let h = parseInt(tmp / 60)
                let p = parseInt(tmp % 60)
                let msg = `Đi muộn ${h > 0 ? `${h}h` : ''} ${p > 0 ? `${p}p` : ''}`
                return { text: `${msg}`, background: ListColorsSystem.yellow30 }
            } else {
                return { text: 'Đã Check In', background: ListColorsSystem.light_green20 }
            }
        }
    }

}