import moment from 'moment';
import _ from 'lodash';
export default {
  getDayOfWeek(d) {
    let numDay = moment(d).day();
    switch (numDay) {
      case 0:
        return 'Chủ nhật';
      case 1:
        return 'Thứ 2';
      case 2:
        return 'Thứ 3';
      case 3:
        return 'Thứ 4';
      case 4:
        return 'Thứ 5';
      case 5:
        return 'Thứ 6';
      case 6:
        return 'Thứ 7';
      default:
        return '';
    }
  },
  timeConvert(num) {
    if(!num && num != 0){
      return "--:--"
    }
    let hours = '00',
      minutes = '00';
    let h = Math.floor(num / 60);
    if (h < 10) {
      hours = '0' + h;
    } else {
      hours = h + '';
    }
    let m = num % 60;
    if (m < 10) {
      minutes = '0' + m;
    } else {
      minutes = m + '';
    }
    return `${hours}:${minutes}`;
  },

  timeConvertToArray(num) {
    let hours = '00',
      minutes = '00';
    let h = Math.floor(num / 60);
    if (h < 10) {
      hours = '0' + h;
    } else {
      hours = h + '';
    }
    let m = num % 60;
    if (m < 10) {
      minutes = '0' + m;
    } else {
      minutes = m + '';
    }
    return {hours, minutes};
  },
  reverseString(str) {
    return str.split('/').reverse().join('/');
  },
  convertData(arr) {
    let dataConvert = [];
    for (let i in arr) {
      let check = false;
      for (let n in dataConvert) {
        if (arr[i]?.date == dataConvert[n].data[0]?.date) {
          dataConvert[n].data.push(arr[i]);
          check = true;
        }
      }
      if (!check && arr[i]?.date) {
        let d = this.reverseString(arr[i]?.date);
        if (d) {
          dataConvert.push({
            title: `${this.getDayOfWeek(d)}, ngày ${moment(d).format(
              'DD/MM/YYYY',
            )}`,
            data: [arr[i]],
          });
        }
      }
    }
    return _.reverse(dataConvert);
  },
};
