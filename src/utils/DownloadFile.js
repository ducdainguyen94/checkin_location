import {
    PermissionsAndroid,
    Platform,
} from 'react-native';

import RNFetchBlob from 'react-native-fetch-blob'

export const downloadFile = async (url, nameFile, callback, progress) => {
    return checkPermission(url, nameFile, callback, progress)
}

const checkPermission = async (url, nameFile, callback, progress) => {
    if (Platform.OS === 'ios') {
        return downloadImage(url, nameFile, callback, progress);
    } else {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Yêu cầu quyền bộ nhớ',
                    message:
                        'Ứng dụng cần được cấp quyền bộ nhớ để tải file',
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return downloadImage(url, nameFile, callback, progress);
            } else {
                // If permission denied then show alert
                alert('Quyền bộ nhớ không được cấp');
            }
        } catch (err) {
        }
    }
};

const downloadImage = (url, nameFile, callback, progress) => {
    let date = new Date();
    let ext = getExtention(nameFile);
    ext = '.' + ext[0];
    const { config, fs } = RNFetchBlob;
    let FileDir = fs.dirs.DownloadDir;
    let options = {
        fileCache: true,
        addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path:
                FileDir +
                '/file_' +
                Math.floor(date.getTime() + date.getSeconds() / 2) +
                ext,
            description: 'Đang tải xuống tập tin...',
        },
    };
    return config(options)
        .fetch('GET', url)
        .progress({ interval: 250 }, (received, total) => {
            if (progress) {
                progress((received / total) * 100)
            }
        })
        .then(res => {
            callback()
        });
};

const getExtention = filename => {
    return /[.]/.exec(filename) ?
        /[^.]+$/.exec(filename) : undefined;
};
