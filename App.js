/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { useEffect } from 'react';
import AppStack from './src/navigation';
import { Alert, Platform, StatusBar, Text } from 'react-native';
import { request, PERMISSIONS, check } from 'react-native-permissions';
import { Provider } from 'react-redux';

import { store, persistor } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import { enableScreens } from 'react-native-screens';
enableScreens();

const App = () => {
  async function checkPermission() {
    try {
      const checkLocation = await check(
        Platform.select(
          {
            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
          }
        ),
      );

      if (checkLocation !== 'granted') {
        Platform.OS == 'android' ? Alert.alert("Quyền vị trí", "Ứng dụng này cần truy cập quyền vị trí để bật tính năng chấm công bằng wifi", [
          {
            text: 'Ok',
            onPress: () => requestPermission(),
          },
        ]) : requestPermission()
      }
    } catch (error) {

    }
  }

  async function requestPermission() {
    try {
      await request(
        Platform.select(
          {
            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
          }),
      );
    } catch (error) {
      console.log('requestPermission error', error)
    }
  }
  
  useEffect(() => {

    checkPermission()

  })

  return (
    <Provider store={store}>
      <PersistGate
        // onBeforeLift={onBeforeLift}
        loading={null}
        persistor={persistor}>
        {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
        <AppStack />
      </PersistGate>
    </Provider>
  );
};
export default App;
